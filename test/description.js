import { assert } from "chai";
import { History } from "../source/index.js";


describe("DESCRIPTION", function() {
	it("one", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0, "change setting1");
		const setting2 = undoable(0, "change setting2")
		const appearance1 = preservable("0");
		const appearance2 = preservable("1");
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setting1.value = 1;
		assert.equal(undoDescription.value, "undo change setting1");
		assert.equal(redoDescription.value, null);
		appearance1.value = "1";
		assert.equal(undoDescription.value, "undo change setting1");
		assert.equal(redoDescription.value, null);
		setting2.value = 2;
		assert.equal(undoDescription.value, "undo change setting2");
		assert.equal(redoDescription.value, null);
		appearance2.value = "2";
		assert.equal(undoDescription.value, "undo change setting2");
		assert.equal(redoDescription.value, null);
		group( () => {
			setting1.value = 11;
			setting2.value = 22;
			appearance1.value = "11";
			appearance2.value = "22";
		}, "change both setting1 and setting2");
		assert.equal(undoDescription.value, "undo change both setting1 and setting2");
		assert.equal(redoDescription.value, null);
		group( () => {
			setting1.value = 111;
			setting2.value = 222;
			appearance1.value = "111";
			appearance2.value = "222";
		}, "change both setting1 and setting2");
		assert.equal(undoDescription.value, "undo change both setting1 and setting2");
		assert.equal(redoDescription.value, null);
		group( () => {
			setting1.value = 1111;
			setting2.value = 2222;
			appearance1.value = "1111";
		}, "change both setting1 and setting2 differently", "coalesce one");
		assert.equal(undoDescription.value, "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription.value, null);
		group( () => {
			setting1.value = 11111;
			setting2.value = 22222;
			appearance2.value = "22222";
		}, "change both setting1 and setting2 differently", "coalesce one");
		assert.equal(undoDescription.value, "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription.value, null);
		group( () => {
			setting1.value = 111111;
			setting2.value = 222222;
		}, "change both setting1 and setting2 without changing appearance", "coalesce two");
		assert.equal(undoDescription.value, "undo change both setting1 and setting2 without changing appearance");	
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2 without changing appearance");
		undo();
		assert.equal(undoDescription.value, "undo change both setting1 and setting2");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2 differently");
		undo();
		assert.equal(undoDescription.value, "undo change both setting1 and setting2");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2");
		undo();
		assert.equal(undoDescription.value, "undo change setting2");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2");
		undo();
		assert.equal(undoDescription.value, "undo change setting1");
		assert.equal(redoDescription.value, "redo change setting2");
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting1");
		undo(); // one extra should have no effect
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting1");
		redo();
		assert.equal(undoDescription.value, "undo change setting1");
		assert.equal(redoDescription.value, "redo change setting2");
		redo();
		assert.equal(undoDescription.value, "undo change setting2");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2");
		redo();
		assert.equal(undoDescription.value, "undo change both setting1 and setting2");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2");
		redo();
		assert.equal(undoDescription.value, "undo change both setting1 and setting2");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2 differently");
		redo();
		assert.equal(undoDescription.value, "undo change both setting1 and setting2 differently");
		assert.equal(redoDescription.value, "redo change both setting1 and setting2 without changing appearance");
		redo();
		assert.equal(undoDescription.value, "undo change both setting1 and setting2 without changing appearance");
		assert.equal(redoDescription.value, null);
		redo(); // one extra should have no effect
		assert.equal(undoDescription.value, "undo change both setting1 and setting2 without changing appearance");
		assert.equal(redoDescription.value, null);
	});

	it("二番", function() {
		const undoLocalizer = function(description) {
			return description + "を取り消す";
		};
		const redoLocalizer = function(description) {
			return description + "をやり直す";
		};
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0, "設定 1 の変更");
		const setting2 = undoable(0, "設定 2 の変更")
		const appearance1 = preservable("0");
		const appearance2 = preservable("1");
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setting1.value = 1;
		assert.equal(undoDescription.value, "設定 1 の変更を取り消す");
		assert.equal(redoDescription.value, null);
		appearance1.value = "1";
		assert.equal(undoDescription.value, "設定 1 の変更を取り消す");
		assert.equal(redoDescription.value, null);
		setting2.value = 2;
		assert.equal(undoDescription.value, "設定 2 の変更を取り消す");
		assert.equal(redoDescription.value, null);
		appearance2.value = "2";
		assert.equal(undoDescription.value, "設定 2 の変更を取り消す");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, "設定 1 の変更を取り消す");
		assert.equal(redoDescription.value, "設定 2 の変更をやり直す");
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "設定 1 の変更をやり直す");
		redo();
		assert.equal(undoDescription.value, "設定 1 の変更を取り消す");
		assert.equal(redoDescription.value, "設定 2 の変更をやり直す");
		redo();
		assert.equal(undoDescription.value, "設定 2 の変更を取り消す");
		assert.equal(redoDescription.value, null);
	});

	it("language chooser declaration", function() {
		function undoLocalizer(description) {
			const position = languageEnum[language.value];
			return undoPrepend[position] + description[position] + undoAppend[position];
		};
		function redoLocalizer(description) {
			const position = languageEnum[language.value];
			return redoPrepend[position] + description[position] + redoAppend[position];
		};
		const languageEnum = {
			"English": 0,
			"日本語": 1
		};
		const undoPrepend = [
			"Undo ",
			""
		];
		const undoAppend = [
			"",
			"を取り消す"
		];
		const redoPrepend = [
			"Redo ",
			""
		];
		const redoAppend = [
			"",
			"をやり直す"
		];
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, ["change setting", "設定の変更"]);
		const language = preservable("English");
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setting.value = 1;
		assert.equal(setting.value, 1);
		assert.equal(language.value, "English");
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		language.value = "日本語";
		assert.equal(setting.value, 1);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, null);
		setting.value = 2;
		assert.equal(setting.value, 2);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(setting.value, 1);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, "設定の変更をやり直す");
		undo();
		assert.equal(setting.value, 0);
		assert.equal(language.value, "English");
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		redo();
		assert.equal(setting.value, 1);
		assert.equal(language.value, "English");
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, "Redo change setting");
		redo();
		assert.equal(setting.value, 2);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, null);
	});

	it("language chooser expression", function() {
		const undoLocalizer = function(changeDescription) {
			const position = languageEnum[language.value];
			return undoPrepend[position] + changeDescription[position] + undoAppend[position];
		};
		const redoLocalizer = function(changeDescription) {
			const position = languageEnum[language.value];
			return redoPrepend[position] + changeDescription[position] + redoAppend[position];
		};
		const languageEnum = {
			"English": 0,
			"日本語": 1
		};
		const undoPrepend = [
			"Undo ",
			""
		];
		const undoAppend = [
			"",
			"を取り消す"
		];
		const redoPrepend = [
			"Redo ",
			""
		];
		const redoAppend = [
			"",
			"をやり直す"
		];
		const { undoable, preservable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, ["change setting", "設定の変更"]);
		const language = preservable("English");
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setting.value = 1;
		assert.equal(setting.value, 1);
		assert.equal(language.value, "English");
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		language.value = "日本語";
		assert.equal(setting.value, 1);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, null);
		setting.value = 2;
		assert.equal(setting.value, 2);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(setting.value, 1);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, "設定の変更をやり直す");
		undo();
		assert.equal(setting.value, 0);
		assert.equal(language.value, "English");
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		redo();
		assert.equal(setting.value, 1);
		assert.equal(language.value, "English");
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, "Redo change setting");
		redo();
		assert.equal(setting.value, 2);
		assert.equal(language.value, "日本語");
		assert.equal(undoDescription.value, "設定の変更を取り消す");
		assert.equal(redoDescription.value, null);
	});

	it("six", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const string = undoable("");
		const setString = function(value) {
			group( () => {
				string.value = value;
			}, "change string to " + value, "string");
		};
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setString("a");
		assert.equal(undoDescription.value, "undo change string to a");
		assert.equal(redoDescription.value, null);
		setString("as");
		assert.equal(undoDescription.value, "undo change string to as");
		assert.equal(redoDescription.value, null);
		setString("asd");
		assert.equal(undoDescription.value, "undo change string to asd");
		assert.equal(redoDescription.value, null);
		setString("asdf");
		assert.equal(undoDescription.value, "undo change string to asdf");
		assert.equal(redoDescription.value, null);
		assert.equal(canUndo, true);
		assert.equal(canRedo, false);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change string to asdf");
		assert.equal(canUndo, false);
		assert.equal(canRedo, true);
		redo();
		assert.equal(undoDescription.value, "undo change string to asdf");
		assert.equal(redoDescription.value, null);
		assert.equal(canUndo, true);
		assert.equal(canRedo, false);
		setString("asdf ");
		assert.equal(undoDescription.value, "undo change string to asdf ");
		assert.equal(redoDescription.value, null);
		assert.equal(canUndo, true);
		assert.equal(canRedo, false);
		setString("asdf z");
		assert.equal(undoDescription.value, "undo change string to asdf z");
		assert.equal(redoDescription.value, null);
		assert.equal(canUndo, true);
		assert.equal(canRedo, false);
		setString("asdf zx");
		assert.equal(undoDescription.value, "undo change string to asdf zx");
		assert.equal(redoDescription.value, null);
		assert.equal(canUndo, true);
		assert.equal(canRedo, false);
		setString("asdf zxc");
		assert.equal(undoDescription.value, "undo change string to asdf zxc");
		assert.equal(redoDescription.value, null);
		assert.equal(canUndo, true);
		assert.equal(canRedo, false);
		setString("asdf zxcv");
		assert.equal(undoDescription.value, "undo change string to asdf zxcv");
		assert.equal(redoDescription.value, null);
		assert.equal(canUndo, true);
		assert.equal(canRedo, false);
		undo();
		assert.equal(undoDescription.value, "undo change string to asdf");
		assert.equal(redoDescription.value, "redo change string to asdf zxcv");
		assert.equal(canUndo, true);
		assert.equal(canRedo, true);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change string to asdf");
		assert.equal(canUndo, false);
		assert.equal(canRedo, true);
	});

	it("seven", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const setSettingOne = function(value) {
			group( () => {
				setting1.value = value;
			}, "change setting one to " + value, "setting1");
		};
		const setSettingTwo = function(value) {
			group( () => {
				setting2.value = value;
			}, "change setting two to " + value, "setting2");
		};
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setSettingOne(1);
		assert.equal(undoDescription.value, "undo change setting one to 1");
		assert.equal(redoDescription.value, null);
		setSettingTwo(2);
		assert.equal(undoDescription.value, "undo change setting two to 2");
		assert.equal(redoDescription.value, null);
		setSettingOne(3);
		assert.equal(undoDescription.value, "undo change setting one to 3");
		assert.equal(redoDescription.value, null);
		setSettingOne(4);
		assert.equal(undoDescription.value, "undo change setting one to 4");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, "undo change setting two to 2");
		assert.equal(redoDescription.value, "redo change setting one to 4");
		undo();
		assert.equal(undoDescription.value, "undo change setting one to 1");
		assert.equal(redoDescription.value, "redo change setting two to 2");
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting one to 1");
		redo();
		assert.equal(undoDescription.value, "undo change setting one to 1");
		assert.equal(redoDescription.value, "redo change setting two to 2");
		redo();
		assert.equal(undoDescription.value, "undo change setting two to 2");
		assert.equal(redoDescription.value, "redo change setting one to 4");
		redo();
		assert.equal(undoDescription.value, "undo change setting one to 4");
		assert.equal(redoDescription.value, null);
	});

	it ("eight", function() { // Should not coalesce after undo, but this one ensures that input stack is also reset
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 4; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		group( () => {
			setting.value = 3;
			appearance.value = 3;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("eight alternate", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 4; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			setting.value = 3;
			appearance.value = 3;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("nine", function() { // null change_description will produce null undoDescription
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, null, true); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("ten", function() { // undefined change_description will produce null undoDescription
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, undefined, true); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("group false change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, false, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, false, true); // should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo false");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("undoable false change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, false, true);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		setting.value = 2; // should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo false");
		assert.equal(setting.value, 0);
	});

	it ("group zero change description is valid and coalesces if key is true", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, 0, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, 0, true); // should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it("maxCount and undoDescription", function() {
		const maxCount = 2;
		const undoLocalizer = function(changeDescription) {
			return "Undo " + changeDescription;
		} 
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, null, maxCount);
		const setting = undoable(0, "change setting");
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		setting.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		undo(); // extra does nothing
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		redo(); // extra does nothing
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
	});

	it("maxCount and redoDescription", function() {
		const maxCount = 2;
		const redoLocalizer = function(changeDescription) {
			return "Redo " + changeDescription;
		} 
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(null, redoLocalizer, maxCount);
		const setting = undoable(0, "change setting");
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		setting.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 1);
		undo(); // extra does nothing
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 2);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		redo(); // extra does nothing
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
	});

	it("maxCount and undoDescription and redoDescription", function() {
		const maxCount = 2;
		const undoLocalizer = function(changeDescription) {
			return "Undo " + changeDescription;
		}
		const redoLocalizer = function(changeDescription) {
			return "Redo " + changeDescription;
		}
		const { undoable, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer, maxCount);
		const setting = undoable(0, "change setting");
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		setting.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 1);
		undo(); // extra does nothing
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 2);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		redo(); // extra does nothing
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
	});

	it("null undoLocalizer", function() {
		const undoLocalizer = null;
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.value = 1;
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		redo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
	});

	it("null redoLocalizer", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = null;
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.value = 1;
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		redo();
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
	});

	it("undefined undoLocalizer", function() { // not enumerable so have to set and check
		const undoLocalizer = undefined;
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.value = 1;
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		redo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
	});

	it("undefined redoLocalizer", function() {
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = undefined;
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		setting.value = 1;
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		redo();
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
	});

	it("undoable zero change description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, 0);
		setting.value = 1;
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		redo();
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
	});

	it("undoable null description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, null);
		setting.value = 1;
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		redo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
	});

	it("undoable undefined description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, undefined);
		setting.value = 1;
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		redo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
	});

	it("undoable false description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, false);
		setting.value = 1;
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo false");
		redo();
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
	});

	it("undoable non-falsy description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "0");
		setting.value = 1;
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		redo();
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
	});

	it("group overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.value = 1;
		}, "change setting override");
		assert.equal(undoDescription.value, "Undo change setting override");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting override");
		redo();
		assert.equal(undoDescription.value, "Undo change setting override");
		assert.equal(redoDescription.value, null);
	});

	it("group zero description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, 0);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		redo();
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
	});

	it("group null description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, null);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		redo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
	});

	it("group false description is valid", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo false");
		redo();
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
	});

	it("group non-falsy description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, "0");
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		redo();
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
	});

	it("group undefined description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.value = 1;
		});
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		redo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
	});

	it("group zero description is valid and overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.value = 1;
		}, 0);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		redo();
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
	});

	it("group null description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.value = 1;
		}, null);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		redo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
	});

	it("group false description is valid and overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.value = 1;
		}, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo false");
		redo();
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
	});

	it("group non-falsy description overrides undoable", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "change setting");
		group( () => {
			setting.value = 1;
		}, "0");
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		undo();
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		redo();
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
	});
});