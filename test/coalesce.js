import { assert } from "chai";
import { History } from "../source/index.js";


describe("COALESCE", function() {
	it ("one", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "setting", true); // these two should coalesce
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "setting", true); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("two", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new History();
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance1.value = 1;
		}, "setting", true); // these two should coalesce
		group( () => {
			setting.value = 2;
			appearance2.value = 2;
		}, "setting", true); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance1.value, 0);
		assert.equal(appearance2.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
	});

	it ("three", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new History();
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance1.value = 1;
		}, "setting style one", true); // these two should not coalesce
		group( () => {
			setting.value = 2;
			appearance2.value = 2;
		},  "setting style two", true); // these two should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 0);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance1.value, 0);
		assert.equal(appearance2.value, 0);
		redo();
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
	});

	it ("four", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new History();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting1.value = 1;
			appearance1.value = 1;
		}, "setting", true); // these two should coalesce
		group( () => {
			setting2.value = 2;
			appearance2.value = 2;
		}, "setting", true); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
		assert.equal(appearance1.value, 0);
		assert.equal(appearance2.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
	});

	it ("five", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0, true);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "setting", true);
		appearance.value = 3; // interrupting
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("five alternate", function() {
		const { undoable, preservable, group, undo, redo, canUndo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0, false);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "setting", true);
		appearance.value = 3; // not interrupting
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("six", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "setting", true);
		setting.value = 3; // this should prevent coalescing
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("seven", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			setting.value = 1;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 3;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("seven alternate", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0, false);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			setting.value = 1;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 3;
		}, "setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("eight", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", true);
		const appearance = preservable(0, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		setting.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("eight alternate", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", true);
		const appearance = preservable(0, false);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		setting.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("nine", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			setting.value = 1;
		}, Math.random() + "", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
		}, Math.random() + "", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 3;
		}, Math.random() + "", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("nine alternate", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0, false);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			setting.value = 1;
		}, Math.random() + "", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
		}, Math.random() + "", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 3;
		}, Math.random() + "", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("undoable coalesce true without changeDescription does coalesce", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		setting.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("alternate undoable coalesce true without changeDescription does coalesce", function() {
		const { undoable, preservable, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, false);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		setting.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("group coalesce true without changeDescription", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, true);
		const grouper = () => {
			setting.value = setting.value + 1;
		};
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it ("alternate group coalesce true without changeDescription", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, true);
		const appearance = preservable(0, false);
		const grouper = () => {
			setting.value = setting.value + 1;
		};
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 0);
		appearance.value = 1; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 1);
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
	});

	it("eleven", function() {
		const { undoable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const setSetting = function(value) {
			group( () => {
				setting.value = value;
			}, null, "setting");
		};
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setSetting(1);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setSetting(2);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		setSetting(3);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
	});

	it("twelve", function() { // preservables do not affect coalescing, when inside coalescing group (they do affect when outside)
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		group( () => {
			setting.value = 1;
			appearance1.value = 1;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
			appearance2.value = 2;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
	});

	it("thirteen", function() { // can coalesce anything with a coalescing key
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting1.value = 1;
		}, null, "various");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			appearance.value = 1;
		}, null, "various");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 0);
		assert.equal(appearance.value, 1);
		group( () => {
			setting2.value = 1;
		}, null, "various");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 1);
		assert.equal(appearance.value, 1);
	});

	it("fourteen", function() { // can coalesce anything with a coalescing key
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting1.value = 1;
		}, null, "various");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			appearance.value = 1;
		}, null, "various");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 0);
		assert.equal(appearance.value, 1);
		group( () => {
			setting2.value = 1;
		}, null, "various");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 1);
		assert.equal(appearance.value, 1);
	});

	it ("fourteen", function() { // coalesce_key can be an object reference // identical to group test number five
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, setting); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", setting); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("fourteen", function() { // coalesce_key can be a Symbol
		const symbol = Symbol("asdf");
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, symbol); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", symbol); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("fifteen", function() { // coalesce_key can be a Symbol, and coalesce both groups and individual undoables
		const symbol = Symbol("asdf");
		const undoLocalizer = function(description) {
			return "undo " + description;
		};
		const redoLocalizer = function(description) {
			return "redo " + description;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, null, symbol);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, symbol);  // these three should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		setting.value = 3; // these three should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", symbol); // these three should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it("undoable coalesce string", function() {
		const { undoable, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, "setting");
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
	});

	it("undoable coalesce symbol", function() {
		const symbol = Symbol("asdf");
		const { undoable, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, symbol);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
	});

	it ("undoable coalesce true and group true with no descriptions", function() {
		const { undoable, undo, group, canUndo, canRedo } = new History();
		const setting = undoable(0, null, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2; // should not coalesce
		}, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce true and group true with description", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		};
		const redoLocalizer = function(description) {
			return "Redo " + description;
		};
		const { undoable, undo, group, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, null, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2; // should not coalesce
		}, "change setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it("undoable coalesce key and group coalesce key", function() {
		const { undoable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, "setting");
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group (() => {
			setting.value = 2;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
	});

	it("undoable coalesce description and group coalesce description", function() {
		const { undoable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", true);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group (() => {
			setting.value = 2;
		}, "change setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
	});

	it("not coalescing with group and descriptions", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		}
		const redoLocalizer = function(description) {
			return "Redo " + description;
		}
		const { undoable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer, 2);
		const setting = undoable(0, "change setting", true);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		group( () => {
			setting.value = 3;
		}, "change setting", false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 0);
		undo(); // extra does nothing
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, "Redo change setting");
		assert.equal(setting.value, 2);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		redo(); // extra does nothing
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
	});

	it("zero description does coalesce", function() {
		const undoLocalizer = function(input) {
			return "Undo " + input;
		};
		const redoLocalizer = function(input) {
			return "Redo " + input;
		};
		const { undoable, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, 0, true);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		assert.equal(setting.value, 0);
	});

	it("non-falsy description does coalesce", function() {
		const undoLocalizer = function(input) {
			return "Undo " + input;
		};
		const redoLocalizer = function(input) {
			return "Redo " + input;
		};
		const { undoable, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0, "0", true);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		assert.equal(setting.value, 0);
	});

	it("group undefined coalescing overrides undoable and does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", true);
		group( () => {
			setting.value = 1;
		}, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it("group null coalescing overrides undoable and does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", true);
		group( () => {
			setting.value = 1;
		}, null, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it("group null coalescing overrides undoable", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", true);
		group( () => {
			setting.value = 1;
		}, null, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it("group false coalescing overrides undoable and does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", true);
		group( () => {
			setting.value = 1;
		}, null, false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it("group zero coalescing overrides undoable and does coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, null, 0);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, 0);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it("group non-falsy coalescing overrides undoable", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, null, "0");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, "0");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce true with null description does coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce true with undefined description does coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, undefined, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("group coalesce true with null description can coalesce if callback is the same", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		const grouper = () => {
			setting.value = setting.value + 1;
		}
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group(grouper, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("group coalesce true with undefined description can coalesce if callback is the same", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		const grouper = () => {
			setting.value = setting.value + 1;
		}
		group(grouper, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group(grouper, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce true with undefined description does coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, undefined, true);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("group coalesce true with undefined description can coalesce if callback is the same", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		const grouper = () => {
			setting.value = setting.value + 1;
		};
		group(grouper, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group(grouper, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("group coalesce true with null description does not coalesce if callback is not the same", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		group( () => {
			setting.value = 1;
		}, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
	});

	it ("group coalesce true with undefined description does not coalesce if callback is not the same", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		group( () => {
			setting.value = 1;
		}, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
	});

	it ("group true coalesce and false description does coalesce", function() {
		const undoLocalizer = function(input) {
			return "Undo " + input;
		};
		const redoLocalizer = function(input) {
			return "Redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, false, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, false, true); // should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo false");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo false");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("group true coalesce and zero description does coalesce", function() {
		const undoLocalizer = function(input) {
			return "Undo " + input;
		};
		const redoLocalizer = function(input) {
			return "Redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, 0, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, 0, true); // should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "Undo 0");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "Redo 0");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("undoable zero coalescing key is valid and coalesces", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, 0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it("group zero coalescing key is valid and coalesces", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, null, 0);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, 0);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable false coalescing key does not coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, null, false);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});
	
	it("group false coalescing key does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
		}, null, false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, null, false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce NaN does not coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", Number.NaN);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("group coalesce NaN does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		const grouper = () => {
			setting.value = setting.value + 1;
		};
		group(grouper, "change setting", Number.NaN);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group(grouper, undefined, true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("group coalesce undefined does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		group( () => {
			setting.value = 1;
		}, undefined, undefined);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, undefined, undefined);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
	});

	it ("group coalesce null does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		group( () => {
			setting.value = 1;
		}, undefined, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, undefined, null);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
	});

	it ("group coalesce false does not coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		group( () => {
			setting.value = 1;
		}, undefined, false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, undefined, false);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
	});

	it ("group coalesce zero does coalesce", function() {
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		group( () => {
			setting.value = 1;
		}, undefined, 0);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		group( () => {
			setting.value = 2;
		}, undefined, 0);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce false does not coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", false);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce undefined does not coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", undefined);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce null does not coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", null);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});

	it ("undoable coalesce zero does coalesce", function() {
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting = undoable(0, "change setting", 0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 0);
		setting.value = 1;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		setting.value = 2;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
	});
});