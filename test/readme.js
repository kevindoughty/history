import { assert } from "chai";
import { History } from "../source/index.js";


describe("README", function() {
	it("it works", function() {
		const undoLocalizer = (changeDescription) => {
			return "Undo " + changeDescription;
		}
		const redoLocalizer = (changeDescription) => {
			return "Redo " + changeDescription;
		}
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		
		const setting = undoable(0, "change setting", true);
		setting.value = 1;
		
		const appearance = preservable(0, true);
		appearance.value = 2;
		setting.value = 3;
		appearance.value = 4;

		group( () => {
			setting.value = 5;
			appearance.value = 6;
		}, "change setting and more", true);
		group( () => {
			setting.value = 7;
			appearance.value = 8;
		}, "change setting and more", true);
		
		undo();
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 4);
		undo();
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 2);
		
		redo();
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 2);

		assert.equal(canUndo.value, true);

		assert.equal(canRedo.value, true);

		assert.equal(undoDescription.value, "Undo change setting");

		assert.equal(redoDescription.value, "Redo change setting and more");


	});
});