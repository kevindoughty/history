import { assert } from "chai";
import { History } from "../source/index.js";
import { computed, effect } from "@preact/signals";


describe("USAGE", function() {
	it("conditional", function() {
		const undoLocalizer = function(description) {
			return "Undo " + description;
		}
		const redoLocalizer = function(description) {
			return "Undo " + description;
		}
		const { undoable, preservable, group, undoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		const setter = function(a,b,c) {
			const message = "change setting to " + a;
			const coalescer = Symbol();
			group( () => {
				setting.value = a;
				appearance1.value = b;
			}, message, coalescer);
			if (c !== undefined) {
				group( () => {
					appearance2.value = c;
				}, null, coalescer); // does not erase undo description
			}
		};
		setter(1,1);
		assert.equal(setting.value, 1);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 0);
		assert.equal(undoDescription.value, "Undo change setting to 1");
		
		setter(2,2,2);
		assert.equal(setting.value, 2);
		assert.equal(appearance1.value, 2);
		assert.equal(appearance2.value, 2);
		assert.equal(undoDescription.value, "Undo change setting to 2");

		setter(3,3);
		assert.equal(setting.value, 3);
		assert.equal(appearance1.value, 3);
		assert.equal(appearance2.value, 2);
		assert.equal(undoDescription.value, "Undo change setting to 3");		
	});


	it("errata", function() {
		const undoLocalizer = (changeDescription) => {
			return "Undo " + changeDescription;
		}
		const redoLocalizer = (changeDescription) => {
			return "Redo " + changeDescription;
		}
		const { undoable, preservable, group, undo, redo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);


		const volume = undoable(0);
		const volumeSymbol = Symbol();
		const crankVolume = function(value) {
			const description = "crank the volume to " + value;
			group( () => {
				volume.value = value;	
			}, description, volumeSymbol);
		}
		crankVolume(10);
		assert.equal(undoDescription.value, "Undo crank the volume to 10");


		group( () => {
			appearance.value = -1;
			setting.value = -1;
			group( () => {
				setting.value = -2;
				appearance.value = -2;
			}, "ignored", Symbol());
		}, null, volumeSymbol); // does not null out undo description
		assert.equal(undoDescription.value, "Undo crank the volume to 10");
		crankVolume(11); // coalesces
		assert.equal(undoDescription.value, "Undo crank the volume to 11");
		undo();
		assert.equal(volume.value, 0);
		assert.equal(redoDescription.value, "Redo crank the volume to 11");


		appearance.value = -3; // lost
		redo();
		assert.equal(appearance.value, -2);
		appearance.value = -4; // lost
		undo();
		redo();
		assert.equal(appearance.value, -2);


		const color = computed( () => {
			if (volume.value < 0) return "black";
			if (volume.value < 6) return "green";
			if (volume.value < 9) return "yellow";
			if (volume.value < 11) return "red";
			return "purple";
		});
		assert.equal(color.value, "purple");
		effect( () => {
			assert.equal(volume.value, 11, "no way don't touch the volume");
		});
		// undo() // don't do it
	});
});