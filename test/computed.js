import { assert } from "chai";
import { History } from "../source/index.js";
import { computed } from "@preact/signals";


describe("COMPUTED", function() {
	it("one", function() {
		const { undoable, preservable } = new History();
		const setting = undoable(0);
		const appearance = preservable(0);
		const derived = computed( () => {
			return setting.value + appearance.value;
		});
		assert.equal(derived.value, 0);
		setting.value = 1;
		assert.equal(derived.value, 1);
		appearance.value = 2;
		assert.equal(derived.value, 3);
	});
});