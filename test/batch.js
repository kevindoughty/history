import { assert } from "chai";
import { History } from "../source/index.js";
import { batch } from "@preact/signals";


describe("BATCH", function() {
	it("wontfix", function() { // Can't fix. Expected behavior. Don't use batch, use group. See first in test/group.js
		const { undoable, undo, canUndo, canRedo } = new History();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
		batch(()=> {
			setting1.value = 1;
			setting2.value = 2;
		});
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 0);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
	});
});