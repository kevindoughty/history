import { assert } from "chai";
import { History } from "../source/index.js";


describe("GROUP", function() {
	it("one", function() { // Compare to first in test/batch.js
		const { undoable, group, undo, canUndo, canRedo } = new History();
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, false);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
		group(()=> {
			setting1.value = 1;
			setting2.value = 2;
		});
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
	});
	
	it("two", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(()=> {
			setting.value = 1;
			appearance.value = 1;
		});
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group(()=> {
			setting.value = 2;
			appearance.value = 2;
		});
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		undo(); // one extra should have no effect
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		redo(); // one extra should have no effect
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it("three", function() {
		const { undoable, preservable, group, undo, redo, canUndo, canRedo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0);
		group(()=> {
			setting.value = 1;
			appearance.value = 1;
		});
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 3;
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 3);
		group(()=> {
			setting.value = 2;
			appearance.value = 2;
		});
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		undo(); // one extra should have no effect
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		redo(); // one extra should have no effect
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("four", function() { // using coalesce_key
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, "setting"); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", "setting"); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("five", function() { // coalesce_key can be an object reference
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, setting); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", setting); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("six", function() { // does not coalesce if keys don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, "setting"); // these two should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change not setting", "not setting"); // these two should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change not setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change not setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change not setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change not setting");
		assert.equal(redoDescription.value, null); 
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("seven", function() { // can coalesce even if group signatures don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting1 = undoable(0);
		const setting2 = undoable(0);
		const appearance1 = preservable(0);
		const appearance2 = preservable(0);
		group( () => {
			setting1.value = 1;
			appearance1.value = 1;
		}, "change settings", "setting"); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change settings");
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 0);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 0);
		group( () => {
			setting2.value = 2;
			appearance2.value = 2;
		}, "change settings", "setting"); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change settings");
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change settings");
		assert.equal(setting1.value, 0);
		assert.equal(setting2.value, 0);
		assert.equal(appearance1.value, 0);
		assert.equal(appearance2.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change settings");
		assert.equal(redoDescription.value, null);
		assert.equal(setting1.value, 1);
		assert.equal(setting2.value, 2);
		assert.equal(appearance1.value, 1);
		assert.equal(appearance2.value, 2);
	});

	it ("eight", function() { // setting undoable between coalescing groups prevents coalescing
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		setting.value = 3; // this should prevent coalescing
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("nine", function() { // setting preservable between coalescing groups prevents coalescing
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 3; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 3);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("nine alternate", function() {
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 3; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 3);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("ten", function() { // should not coalesce after redo
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, "setting"); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", "setting"); // these two should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		group( () => {
			setting.value = 3;
			appearance.value = 3;
		}, "change setting again", "setting"); // should not coalesce after redo
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting again");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting again");
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("eleven", function() { // should not coalesce after undo
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, true);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 4; // interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		group( () => {
			setting.value = 3;
			appearance.value = 3;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});
	
	it ("eleven alternate", function() {
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, canUndo, canRedo } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		appearance.value = 4; // not interrupting
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 4);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		group( () => {
			setting.value = 3;
			appearance.value = 3;
		}, null, "setting");
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(setting.value, 3);
		assert.equal(appearance.value, 3);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
	});

	it ("twelve", function() { // undefined coalesce_key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting"); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting"); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("thirteen", function() { // null coalesce_key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", null); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", null); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});
	
	it ("fourteen", function() { // boolean false coalesce_key will not coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		assert.equal(canUndo.value, false);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", false); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", false); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("fifteen", function() { // boolean true coalesce_key will use change_description to coalesce
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", true); // should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting", true); // should coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it ("sixteen", function() { // should not coalesce even with boolean true coalesce_key if change_descriptions don't match
		const undoLocalizer = function(input) {
			return "undo " + input;
		};
		const redoLocalizer = function(input) {
			return "redo " + input;
		};
		const { undoable, preservable, group, undo, redo, canUndo, canRedo, undoDescription, redoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0);
		const appearance = preservable(0);
		group( () => {
			setting.value = 1;
			appearance.value = 1;
		}, "change setting", true);
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		group( () => {
			setting.value = 2;
			appearance.value = 2;
		}, "change setting again", true); // should not coalesce
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting again");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
		undo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting again");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		undo();
		assert.equal(canUndo.value, false);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, null);
		assert.equal(redoDescription.value, "redo change setting");
		assert.equal(setting.value, 0);
		assert.equal(appearance.value, 0);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, true);
		assert.equal(undoDescription.value, "undo change setting");
		assert.equal(redoDescription.value, "redo change setting again");
		assert.equal(setting.value, 1);
		assert.equal(appearance.value, 1);
		redo();
		assert.equal(canUndo.value, true);
		assert.equal(canRedo.value, false);
		assert.equal(undoDescription.value, "undo change setting again");
		assert.equal(redoDescription.value, null);
		assert.equal(setting.value, 2);
		assert.equal(appearance.value, 2);
	});

	it("nested group call order one", function() {
		const { undoable, group, undo, canUndo } = new History();
		const setting = undoable(0);
		group( () => {
			setting.value = 1;
			group( () => {
				setting.value = 2;
			});
		});
		assert.equal(setting.value, 2);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
		
	});

	it("nested group call order two", function() {
		const { undoable, group, undo, canUndo } = new History();
		const setting = undoable(0);
		group( () => {
			group( () => {
				setting.value = 1;
			});
			setting.value = 2;
		});
		assert.equal(setting.value, 2);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
	});

	it("nesting outer group wins description", function() {
		const undoLocalizer = function(input) {
			return "Undo " + input;
		};
		const redoLocalizer = function(input) {
			return "Redo " + input;
		};
		const { undoable, group, undo, canUndo, undoDescription } = new History(undoLocalizer, redoLocalizer);
		const setting = undoable(0,"change undoable");
		group( () => {
			group( () => {
				setting.value = 2;
			}, "change inner");
			setting.value = 1;
		}, "change outer");
		assert.equal(setting.value, 1);
		assert.equal(canUndo.value, true);
		assert.equal(undoDescription.value, "Undo change outer");
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
		assert.equal(undoDescription.value, null);
	});

	it("nesting outer group wins coalescing false", function() {
		const { undoable, group, undo, canUndo } = new History();
		const setting = undoable(0, null, "undoable coalesce");
		group( () => {
			setting.value = 1;
			group( () => {
				setting.value = 2;
			}, null, "inner coalesce");
		}, null, false);
		group( () => {
			setting.value = 3;
			group( () => {
				setting.value = 4;
			}, null, "inner coalesce");
		}, null, false);
		assert.equal(setting.value, 4);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 2);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
	});

	it("nesting outer group wins coalescing true", function() {
		const { undoable, group, undo, canUndo } = new History();
		const setting = undoable(0, null, false);
		group( () => {
			setting.value = 1;
			group( () => {
				setting.value = 2;
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting.value = 3;
			group( () => {
				setting.value = 4;
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(setting.value, 4);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
	});

	it("nesting outer group wins coalescing false two", function() {
		const { undoable, group, undo, canUndo } = new History();
		const setting = undoable(0, null, "undoable coalesce");
		group( () => {
			setting.value = 1;
			group( () => {
				setting.value = 2;
			}, null, "inner coalesce");
			group( () => {
				setting.value = 3;
			}, null, "inner coalesce");
		}, null, false);
		group( () => {
			setting.value = 4;
			group( () => {
				setting.value = 5;
			}, null, "inner coalesce");
			group( () => {
				setting.value = 6;
			}, null, "inner coalesce");
		}, null, false);
		assert.equal(setting.value, 6);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 3);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
	});

	it("nesting outer group wins coalescing true", function() {
		const { undoable, group, undo, canUndo } = new History();
		const setting = undoable(0, null, false);
		group( () => {
			setting.value = 1;
			group( () => {
				setting.value = 2;
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting.value = 3;
			group( () => {
				setting.value = 4;
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(setting.value, 4);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
	});

	it("nesting outer group wins coalescing true two", function() {
		const { undoable, group, undo, canUndo } = new History();
		const setting = undoable(0, null, false);
		group( () => {
			setting.value = 1;
			group( () => {
				setting.value = 2;
			}, null, false);
			group( () => {
				setting.value = 3;
			}, null, false);
		}, null, "outer coalesce");
		group( () => {
			setting.value = 4;
			group( () => {
				setting.value = 5;
			}, null, false);
			group( () => {
				setting.value = 6;
			}, null, false);
		}, null, "outer coalesce");
		assert.equal(setting.value, 6);
		assert.equal(canUndo.value, true);
		undo();
		assert.equal(setting.value, 0);
		assert.equal(canUndo.value, false);
	});
});