import { assert } from "chai";
import { History } from "../source/index.js";


function isFunction(w) {
	return w && {}.toString.call(w) === "[object Function]";
}


describe("API", function() {
	it("isFunction", function() {
		const test = function() {};
		assert(isFunction(test));
		assert(isFunction(function() {}));
		assert(isFunction(() => {}));
		assert(!isFunction({}));
		assert(!isFunction("[object Function]"));
	});

	it("undoable", function() {
		const { undoable } = new History();
		assert(isFunction(undoable));
	});
	it("preservable", function() {
		const { preservable } = new History();
		assert(isFunction(preservable));
	});
	it("group", function() {
		const { group } = new History();
		assert(isFunction(group));
	});
	it("undo", function() {
		const { undo } = new History();
		assert(isFunction(undo));
	});
	it("redo", function() {
		const { redo } = new History();
		assert(isFunction(redo));
	});
	it("canUndo", function() {
		const { canUndo } = new History();
		assert(typeof canUndo !== "undefined" && canUndo !== undefined && canUndo !== null);
		assert(canUndo.value === false);
	});
	it("canRedo", function() {
		const { canRedo } = new History();
		assert(typeof canRedo !== "undefined" && canRedo !== undefined && canRedo !== null);
		assert(canRedo.value === false);
	});
	it("undoDescription", function() {
		const { undoDescription } = new History();
		assert(typeof undoDescription !== "undefined" && undoDescription !== undefined && undoDescription !== null);
		assert.equal(undoDescription.value, null);
	});
	it("redoDescription", function() {
		const { redoDescription } = new History();
		assert(typeof redoDescription !== "undefined" && redoDescription !== undefined && redoDescription !== null);
		assert.equal(redoDescription.value, null); 
	});
	it("Object.keys", function() {
		const history = new History();
		assert.equal(Object.keys(history).length, 9);
	});
	it("Object.getOwnPropertyNames", function() {
		const history = new History();
		assert.equal(Object.getOwnPropertyNames(history).length, 9);
	});
});