import { assert } from "chai";
import { History } from "../source/index.js";
import { computed, effect } from "@preact/signals";


describe("EFFECT", function() {
	it("one", function() {
		const { undoable, preservable, undo, redo } = new History();
		const setting = undoable(0);
		const appearance = preservable(0);
		const derived = computed( () => {
			return setting.value + appearance.value;
		});
		let counter = 0;
		effect(() => {
			counter = counter + derived.value;
		});
		assert.equal(counter, 0);
		appearance.value = 1;
		assert.equal(counter, 1);
		setting.value = 10;
		assert.equal(counter, 12);
		appearance.value = 100;
		assert.equal(counter, 122);
		setting.value = 1000;
		assert.equal(counter, 1222);
		appearance.value = 10000;
		assert.equal(counter, 12222);
		undo(); // Changes in undo are grouped.
		assert.equal(counter, 12332);
		undo(); // Changes in undo are grouped.
		assert.equal(counter, 12333);
		redo(); // Changes in redo are grouped.
		assert.equal(counter, 12344);
		redo(); // Changes in redo are grouped.
		assert.equal(counter, 13444);
	});
});