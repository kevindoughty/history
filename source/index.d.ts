import { ReadonlySignal } from "@preact/signals";

declare module "@kvndy/history" {

	export class Undoable<T = any> {
		constructor(initialValue?: T, changeDescription?: any, coalescing?: any);
		valueOf(): T;
		toString(): string;
		toJSON(): T;
		peek(): T;
		get value(): T;
		set value(value: T);
	}

	export class Preservable<T = any> {
		constructor(initialValue?: T, interrupting?: boolean);
		valueOf(): T;
		toString(): string;
		toJSON(): T;
		peek(): T;
		get value(): T;
		set value(value: T);
	}

	export type Localizer = (changeDescription: unknown) => string | null;

	export class History {
		constructor(undoLocalizer?: Localizer, redoLocalizer?: Localizer, maxDepth?: number );
		undoable(initialValue?: any, changeDescription?: any, coalescing?: any): Undoable;
		preservable(initialValue?: any, interrupting?: boolean): Preservable;
		group( callback: Function, changeDescription?: any, coalescing?: any): void;
		undo(): void;
		redo(): void;
		canUndo: ReadonlySignal;
		canRedo: ReadonlySignal;
		undoDescription: ReadonlySignal;
		redoDescription: ReadonlySignal;
	}
}