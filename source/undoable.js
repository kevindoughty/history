import { signal } from "@preact/signals";

/**
 * An undoable object
 * @typedef {function} Undoable
 * @param {*} initialValue
 * @param {*} changeDescription
 * @param {*} coalescing
 */
function Undoable(initialValue, changeDescription, coalescing, context) { // private
	const _signal = signal(initialValue);
	const id = context.registerUndoable();

	const registerUndo = function(instance, value) {
		const undoer = function() {
			context.group( () => {
				const original = _signal.value;
				_signal.value = value;
				registerUndo(instance, original);
			}, changeDescription);
		}
		context.addUndoerForKey(undoer, id);
	};

	const setter = function(instance, value) {
		let actual_coalescing = coalescing;
		if (coalescing === true && (changeDescription === null || changeDescription === undefined)) {
			actual_coalescing = instance;
		}
		const original = _signal.value;
		if (value !== original) {
			context.group( () => {
				_signal.value = value;
				registerUndo(instance, original);
			}, changeDescription, actual_coalescing);
		}
	};

	this.value = undefined; // required for typescript
	Object.defineProperty(this, "value", { // public
		get() {
			return _signal.value;
		},
		set(value) {
			setter(this, value);
		},
		enumerable:true,
		configurable: false
	});
	
	this.peek = function() { // public
		return _signal.peek();
	}
}
Undoable.prototype = Object.create(null);

Undoable.prototype.valueOf = function () {
	return this.value;
};

Undoable.prototype.toString = function () {
	return this.value + "";
};

Undoable.prototype.toJSON = function () {
	return this.value;
};

function undoable(context) { // private
	return function(initialValue, changeDescription, coalescing) { // public	
		return new Undoable(initialValue, changeDescription, coalescing, context); // private
	};
}

export { undoable };