import { signal, batch } from "@preact/signals";


function Context(undoLocalizer, redoLocalizer, maxCount) { // private
	this.next = [[],Object.create(null),[]];
	this.current = this.next;
	this.undo_stack = signal( [] );
	this.redo_stack = signal( [] );
	this.preservables = [];
	this.befores = [];
	this.afters = [];
	this.is_coalescing_group = false;
	this.current_coalesce_key = null;
	this.previous_coalesce_key = null;
	this.undoLocalizer = undoLocalizer || (() => null);
	this.redoLocalizer = redoLocalizer || (() => null);
	this.undo_descriptions = signal( [] );
	this.redo_descriptions = signal( [] );
	this.counter = 0; // Used in addUndoerForKey and could be replaced with Javascript Map 
	this.depth = 0;
	this.maxCount = maxCount;
	if (!Number.isInteger(this.maxCount) || this.maxCount < 0) this.maxCount = Infinity;
}

Context.prototype.registerPreservable = function(preservable, registerBefore, registerAfter) {
	this.preservables.push(preservable);
	this.befores.push(registerBefore);
	this.afters.push(registerAfter);
}

Context.prototype.registerUndoable = function() {
	return this.counter++;
}

Context.prototype.beforeChange = function(direction) { // undoables only
	const undo_previous = this.undo_stack.value;
	const undo_length = undo_previous.length;
	const redo_previous = this.redo_stack.value;
	const redo_length = redo_previous.length;
	if (direction < 0) { // undo
		if (undo_length > 0) {
			const undo_next = [ ...undo_previous ];
			this.current = undo_next.pop();
			this.undo_stack.value = undo_next;
			this.next = [[],Object.create(null),[]];
			const redo_next = [ ...this.redo_stack.value, this.next ];
			this.redo_stack.value = redo_next;
			
			this.current_coalesce_key = null;
			this.previous_coalesce_key = null;

			const undo_descriptions_next = [ ...this.undo_descriptions.value ];
			const current_undo_descriptions = undo_descriptions_next.pop();
			this.undo_descriptions.value = undo_descriptions_next;
			this.redo_descriptions.value = [ ...this.redo_descriptions.value, current_undo_descriptions ];

		} else this.current = [[],Object.create(null),[]];
	} else if (direction > 0) { // redo
		if (redo_length > 0) {
			const redo_next = [ ...redo_previous ];
			this.current = redo_next.pop();
			this.redo_stack.value = redo_next;
			this.next = [[],Object.create(null),[]];
			const undo_next = [ ...this.undo_stack.value, this.next ];
			this.undo_stack.value = undo_next;
			
			this.current_coalesce_key = null;
			this.previous_coalesce_key = null;

			const redo_descriptions_next = [ ...this.redo_descriptions.value ];
			const current_redo_descriptions = redo_descriptions_next.pop();
			this.redo_descriptions.value = redo_descriptions_next;
			this.undo_descriptions.value = [ ...this.undo_descriptions.value, current_redo_descriptions ];

		} else this.current = [[],Object.create(null),[]];
	} else {
		if (!this.should_coalesce()) {
			this.next = [[],Object.create(null),[]];
		}
	}
	if (!this.should_coalesce() && direction === 0) {
		this.preservables.forEach( (item, index) => {
			const registerBefore = this.befores[index];
			const value = item.value;
			const invocation = registerBefore(value);
			this.next[0].push(invocation);
		});
		this.redo_descriptions.value = [];
	}
}

Context.prototype.afterChange = function(direction, change_description) {
	if (direction === 0) { // neither undo nor redo
		if (this.should_coalesce()) {
			const next_next = this.next.slice(0);
			next_next.pop();
			next_next[2] = [];
			this.next = next_next;
		}
		this.preservables.forEach( (item, index) => {
			const registerAfter = this.afters[index];
			const value = item.value;
			const invocation = registerAfter(value);
			this.next[2].push(invocation);
		});
		if (this.maxCount > 0) {
			if (this.should_coalesce()) {
				const next_stack = this.undo_stack.value.slice(0);
				next_stack.pop();
				next_stack.push(this.next);
				this.undo_stack.value = next_stack;
				const next_input = this.undo_descriptions.value.slice(0);
				if (change_description !== undefined && change_description !== null) {
					next_input.pop();
					next_input.push(change_description);
				}
				this.undo_descriptions.value = next_input;
			} else {
				const undo_stack_next = [ ...this.undo_stack.value ];
				const undo_descriptions_next = [ ...this.undo_descriptions.value ];
				if (undo_stack_next.length === this.maxCount) {
					undo_stack_next.shift();
					undo_descriptions_next.shift();
				}
				if (undo_stack_next.length < this.maxCount) {
					undo_stack_next.push(this.next);
					undo_descriptions_next.push(change_description);
				}
				this.undo_stack.value = undo_stack_next;
				this.redo_stack.value = [];
				this.undo_descriptions.value = undo_descriptions_next;
				this.redo_descriptions.value = [];
			}
		}
	}
}

Context.prototype.wrapper = function(direction, change_description, callback) {
	if (this.depth === 0) {
		this.beforeChange(direction);
	}
	this.depth++;
	const result = batch(callback);
	this.depth--;
	if (this.depth === 0) {
		this.afterChange(direction, change_description);
	}
	return result;
}

Context.prototype.group = function(callback, change_description, coalescing) {
	if (coalescing === true && (change_description === null || change_description === undefined)) {
		coalescing = callback;
	}
	const is_coalescing = coalescing !== null && coalescing !== undefined && coalescing !== false && (coalescing !== true || (change_description !== null && change_description !== undefined));
	if (this.depth === 0) {
		if (is_coalescing) {
			this.begin_coalescing_group();
			this.previous_coalesce_key = this.current_coalesce_key;
			if (coalescing === true) this.current_coalesce_key = change_description;
			else this.current_coalesce_key = coalescing;
		} else this.current_coalesce_key = null;
	}
	const result = this.wrapper(0, change_description, callback);
	if (this.depth === 0) {
		if (is_coalescing) {
			this.end_coalescing_group();
		}
	}
	return result;
}

Context.prototype.should_coalesce = function() {
	if (this.is_coalescing_group && this.current_coalesce_key !== null && this.current_coalesce_key === this.previous_coalesce_key) {
		return true;
	}
	return false;
}

Context.prototype.begin_coalescing_group = function() {
	this.is_coalescing_group = true;
}

Context.prototype.end_coalescing_group = function() {
	this.is_coalescing_group = false;
}

Context.prototype.addUndoerForKey = function(undoer, key) {
	const existing = this.next[1][key]; // don't overwrite existing when coalescing
	if (existing === undefined) this.next[1][key] = undoer;
}

Context.prototype.make_non_coalescing_change = function() {
	if (!this.is_coalescing_group) {
		this.current_coalesce_key = null;
	}
}

Context.prototype.undo = function() {
	const context = this;
	const direction = -1;
	this.wrapper(direction, null, () => {
		context.current[0].forEach( undoer => {
			const invocation = undoer(direction);
			context.next[0].push(invocation);
		});
		Object.keys(context.current[1]).forEach( key => {
			const undoer = context.current[1][key];
			undoer(direction);
		});
		context.current[2].forEach( undoer => {
			const invocation = undoer(direction);
			context.next[2].push(invocation);
		});
	});
}

Context.prototype.redo = function() {
	const context = this;
	const direction = 1;
	this.wrapper(direction, null, () => {
		context.current[0].forEach( redoer => {
			const invocation = redoer(direction);
			context.next[0].push(invocation);
		});
		Object.keys(context.current[1]).forEach( key => {
			const redoer = context.current[1][key];
			redoer(direction);
		});
		context.current[2].forEach( redoer => {
			const invocation = redoer(direction);
			context.next[2].push(invocation);
		});
	});
}

Context.prototype.canUndo = function() {
	return this.undo_stack.value.length > 0;
}

Context.prototype.canRedo = function() {
	return this.redo_stack.value.length > 0;
}

Context.prototype.undoDescription = function() {
	const input = this.undo_descriptions.value;
	const length = input.length;
	const value = length ? input[length-1] : null;
	if (value === null || value === undefined) return null;
	return this.undoLocalizer(value);
}

Context.prototype.redoDescription = function() {
	const input = this.redo_descriptions.value;
	const length = input.length;
	const value = length ? input[length-1] : null;
	if (value === null || value === undefined) return null;
	return this.redoLocalizer(value);
}

export { Context };