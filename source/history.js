import { computed } from "@preact/signals";
import { undoable } from "./undoable.js";
import { preservable } from "./preservable.js";
import { Context } from "./context.js";

/**
 * An optional callback passed to the History contstructor.
 * Generates localized strings to describe changes.
 * @typedef {function} Localizer
 * @param {*} changeDescription
 * @returns {(string | null)} A localized string describing a change
 */

/**
 * An object that provides change and undo stack management
 * @constructor
 * @param {Localizer} undoLocalizer
 * @param {Localizer} redoLocalizer
 * @param {number} maxCount
 * @returns Primitives used for managing the undo stack
 */
function History(undoLocalizer, redoLocalizer, maxCount) { // public constructor, the default export
	const context = new Context(undoLocalizer, redoLocalizer, maxCount);
	this.undoable = undoable(context); // public
	this.preservable = preservable(context); // public
	this.group = group(context); // public
	this.undo = undo(context); // public
	this.redo = redo(context); // public
	this.canUndo = canUndo(context); // public
	this.canRedo = canRedo(context); // public
	this.undoDescription = undoDescription(context); // public
	this.redoDescription = redoDescription(context); // public
}
History.prototype = Object.create(null);

function group(context) { // private
/**
 * A function that permits batch changes
 * @param {function} callback
 * @param {*} changeDescription
 * @param {*} coalescing
 */
	return function(callback, changeDescription, coalescing) { // public
		context.group(callback, changeDescription, coalescing);
	};
}

const undo = function(context) { // private
/**
 * A function which restores state to its previous value
 */
	return function() { // public
		context.undo();
	}
}

const redo = function(context) { // private
/**
 * A function which restores state to its next value
 */
	return function() { // public
		context.redo();
	}
}

/**
 * A Preact ReadonlySignal
 * @typedef {object} ReadonlySignal
 */

const canUndo = function(context) { // private
/**
 * A Preact ReadonlySignal whose value getter provides if undo is possible
 */
	return computed( () => { // public
		return context.canUndo();
	});
}

const canRedo = function(context) { // private
/**
 * A Preact ReadonlySignal whose value getter provides if redo is possible
 */
	return computed (() => { // public
		return context.canRedo();
	});
}

const undoDescription = function(context) { // private
/**
 * A Preact ReadonlySignal whose value getter provides a change description
 */
	return computed( () => { // public
		return context.undoDescription();
	});
}

const redoDescription = function(context) { // private
/**
 * A Preact ReadonlySignal whose value getter provides a change description
 */
	return computed (() => { // public
		return context.redoDescription();
	});
}

export { History };