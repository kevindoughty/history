import { signal } from "@preact/signals";

/**
 * An preservable object
 * @typedef {function} Preservable
 * @param {*} initialValue
 * @param {boolean} interrupting
 */
function Preservable(initialValue, interrupting, context) { // private
	const _signal = signal(initialValue);

	const registerBefore = function(value) {
		const invocation = function(direction) {
			if (direction < 0) { // undo
				_signal.value = value;
			}
			return registerBefore(value);
		};
		return invocation;
	};
	
	const registerAfter = function(value) {
		const invocation = function(direction) {
			if (direction > 0) { // redo
				_signal.value = value;
			}
			return registerAfter(value);
		};
		return invocation;
	};
	
	this.value = undefined; // required for typescript
	Object.defineProperty(this, "value", { // public
		get() {
			return _signal.value;
		},
		set(value) {
			if (interrupting) context.make_non_coalescing_change();
			_signal.value = value;
		},
		enumerable: true,
		configurable: false
	});

	this.peek = function() { // public
		return _signal.peek();
	}

	context.registerPreservable(this, registerBefore, registerAfter);
}
Preservable.prototype = Object.create(null);

Preservable.prototype.valueOf = function () { // public
	return this.value;
};

Preservable.prototype.toString = function () { // public
	return this.value + "";
};

Preservable.prototype.toJSON = function () { // public
	return this.value;
};

function preservable(context) { // private
/**
 * A function that returns a preservable object
 * @param {*} initialValue
 * @param {boolean} interrupting
 * @returns A preservable object
 */
	return function(initialValue, interrupting) { // public
		return new Preservable(initialValue, interrupting, context); // private
	};
}

export { preservable };