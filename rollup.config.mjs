import terser from "@rollup/plugin-terser";

export default {
	input: "source/index.js",
	output: [
		{
			file: "dist/history.common.js",
			format: "cjs",
			name: "History"
		},
		{
			file: "dist/history.min.js",
			format: "iife",
			name: "History",
			plugins: [terser()],
			globals: {
				"@preact/signals": "signals"
			}
		},
		{
			file: "dist/history.umd.js",
			format: "umd",
			name: "History",
			globals: {
				"@preact/signals": "signals"
			}
		},
		{
			file: "dist/history.module.js",
			format: "es",
			name: "History"
		}
	],
	external: ["@preact/signals"]
};